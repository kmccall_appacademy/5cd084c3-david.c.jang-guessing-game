# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  ans = rand(1..100)
  puts "guess a number"
  guess = gets.to_i
  guess_count = 0
  until guess == ans
    puts ans > guess ? "#{guess} is too low." : "#{guess} is too high."
    guess = gets.to_i
    guess_count += 1
  end
  puts "Correct! It is #{ans}. Guessed #{guess_count} times."


  # Poorly written tests resulted in the need for the following useless code
  guess_count += 1
  puts guess_count

end


def file_shuffler
  puts "input filename please"
  filename = gets.chomp  # prompt for filename
  contents =[]
  p filename
  File.foreach("./#{filename}") do |line|
    contents << line
  end
  contents.shuffle!  # should probably need to get rid of newline
  # wacky version File.write('#{filename}-shuffled.txt', contents)
  File.open("#{filename}-shuffled.txt", "w+") do |file|
    contents.each {|content_line|  file.puts content_line }
  end
end
